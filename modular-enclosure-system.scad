/* [Part definition] */

// Kind of part.
part_type = "connect222";  // [stem, connect22, connect33, connect44, connect222, connect2233, connect3333, connect4444, connect33334, connect444444, brace2, stop2, hinge2, brace3, edge2, edge3, edge4, cable_tie_insert, test]

// Length of edge parts as a multiple of unit sections.
num_sections = 8; // [2:20]

// Use snap-fit instead of screwed braces.
snap_fit_braces = false;

// Door attachment with snap-fit pegs instead of screws and nuts.
snap_fit_doors = false;

// Slots for inserts.
insert_slots = false;

// Conduit for cable tie.
cable_tie_conduits = true;

// Number of walls that are door frames.
door_frame_walls = 0; // [0:2]

// Rotate part to optimal printing orientation.
printing_orientation = true;


/* [Cable hole dimensions] */

// Number of centered cable hole lengths.
cable_hole_cables = 0; // [0:10]

// Fraction of the edge piece where the center of the cable hole will be.
cable_hole_fractional_position = 0.5; // [0.0:0.1:1.0]


/* [Section dimensions] */

// Length unit for linear modules.
section_length = 20.0; // [15:30]

// Section width of linear modules.
side_length = 16.0; // [12:24]

// Side projected length of the corner that is cut off from linear modules in diagonal.
cut_side_length = 5.0; // [1:10]

// Side projected length of the corner that is cut off from linear modules in diagonal in the inside.
inside_cut_side_length = 5.7; // [0:0.1:10]

// Cable hole diameter.
cable_hole_diameter = 8.0; // [2:0.1:10]

// Cable hole cover width.
cable_hole_cover_width = 1.0; // [0.4:0.1:10]


/* [Insert dimensions] */

// Thickness of inserts.
insert_thickness = 1.5; // [0.1:0.1:4]

// Length of inserts.
insert_length = 5.0; // [3.0:0.1:10]

// Length of insert retainer.
insert_retainer_length = 1.5; // [0.5:0.1:5]

// Depth of insert retainer.
insert_retainer_depth = 1.5; // [0.5:0.1:2]

// Thickness of the insert bases.
insert_base_thickness = 1.5; // [1:0.1:4]

// Rotation of the insert's add-on.
insert_addon_rotation = 0; // [0:45:135]


/* [Brace dimensions] */

// Thickness of braces.
brace_thickness = 1.8; // [1:0.1:4]


/* [Stem dimensions] */

// Thickness of stems.
stem_side_length = 5.0; // [1:10]

// Length of stems.
stem_length = 32.0; // [10:64]

// Thickness of stem stops (the wall that serves as the end for stem insertion).
stem_stop_depth = 4.0;  // [1:0.1:8]

/* [Wall insert dimensions] */

// Thickness of the wall panels.
sheet_width = 3.0; // [1:0.1:5]

// How deep an indentation to make to hold the wall panels.
sheet_indent = 3.0; // [1:0.1:5]


/* [Screw dimensions] */

// Nominal brace screw diameter.
screw_diameter = 2.8; // [1:0.1:5]
    
// Total brace screw length.
screw_length = 5.0; // [1:0.1:30]

// Diameter of the brace screw head.
head_diameter = 6.0; // [1:0.1:10]
    

/* [Hinge dimensions] */

// Width of terminal hinge knuckles.
side_knuckle_width = 2.0; // [1:0.5:5]

// Number of non-terminal hinge knuckles.
num_knuckles = 7; // [1:13]


/* [Nut dimensions] */

// Width of nuts across flats.
nut_flat_width = 5.4; // [0.5:0.1:10]

// Thickness of nuts.
nut_depth = 2.60; // [0.5:0.1:10]

// Thickness of walls holding nuts.
nut_holder_wall_width = 0.5; // [0.1:0.1:1]


/* [Stop magnet dimensions] */

// Diameter of door stop magnets.
magnet_diameter = 6.0; // [0.5:0.1:10]

// Thickness of door stop magnets.
magnet_depth = 2.0; // [0.5:0.1:10]

// Thickness of walls holding magnets.
magnet_holder_wall_width = 0.5; // [0.1:0.1:1]


/* [Snap fit peg dimensions] */

// Number of tolerances for pegs for the snap-fit option.
snap_fit_peg_tolerances = 3; // [1:5]


/* [Cable tie insert] */

// Width of the holes to pass the cable tie.
cable_tie_hole_width = 2.0; // [2.0:0.1:10.0]

// Height of the holes to pass the cable tie.
cable_tie_hole_height = 1.0; // [1.0:0.1:5.0]

// Diameter of the cable tie holder stem.
cable_tie_holder_diameter = 1.0; // [0.4:0.1:2.0]

// Width of the cable tie holder stem.
cable_tie_holder_width = 1.5; // [0.8:0.1:4.0]



/* [Tolerances] */

// Tolerance between sliding parts.
tolerance = 0.15; // [0:0.01:0.5]

// Tolerance for screw holes.
screw_tolerance = 0.1; // [0:0.01:0.5]

// Tolerance between different hinge parts.
hinge_tolerance = 0.3; // [0:0.01:0.5]

// Minimum printable wall depth.
hinge_wall_allowance = 0.4; // [0.01:0.01:1]

// Minimum arc angle.
$fa=1.0; // [1:20]

// Minimum arc segment length.
$fs=0.05; // [0.01:0.01:2.0]


module screw_hole(
    screw_diameter=screw_diameter,
    screw_length=screw_length,
    head_diameter=head_diameter,
    screw_tolerance=screw_tolerance,
) {
    screw_semi_tolerance = screw_tolerance / 2;
    hole_radius = screw_diameter / 2;
    head_radius = head_diameter / 2;
    head_height = (head_diameter - screw_diameter) / 2;
    
    translate([0, 0, -screw_semi_tolerance]) {
        cylinder(h=screw_length + screw_tolerance, r=hole_radius + screw_semi_tolerance);
        cylinder(h=head_height + screw_tolerance, r1=head_radius + screw_semi_tolerance, r2=hole_radius + screw_semi_tolerance);                
    }         
}


module snap_fit_peg(
    screw_diameter=screw_diameter,
    screw_tolerance=screw_tolerance,
    tolerance=tolerance,
    snap_fit_peg_tolerances=snap_fit_peg_tolerances,
) {
    screw_semi_tolerance = screw_tolerance / 2;
    semi_tolerance = tolerance / 2;
    
    rotate_extrude(convexity=1)
    polygon([
        [screw_diameter / 2 - screw_semi_tolerance, -semi_tolerance],
        [screw_diameter / 2 - screw_semi_tolerance, tolerance * snap_fit_peg_tolerances],
        [screw_diameter / 2 - screw_semi_tolerance - semi_tolerance, tolerance * snap_fit_peg_tolerances + semi_tolerance],
        [0, tolerance * snap_fit_peg_tolerances + semi_tolerance],
        [0, -semi_tolerance],            
    ]);
}


module linear_run2(
    num_sections=12,
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_length=screw_length,
    head_diameter=head_diameter,
    screw_tolerance=screw_tolerance,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
    brace_thickness=brace_thickness,
    terminal=true,
    skip_holes=false,
) {
    semi_side = side_length / 2;
    corrected_side = semi_side - (terminal ? brace_thickness + tolerance : 0);
    semi_sheet = sheet_width / 2;
    semi_tolerance = tolerance / 2;
    screw_semi_tolerance = screw_tolerance / 2;
    semi_stem = stem_side_length / 2;
    corrected_cut = max(0, cut_side_length - sin(45) * (terminal ? brace_thickness + tolerance : 0));
    semi_cut = corrected_cut / 2;
    
    // Main body and holes.
    difference() {
        // Profile extrusion.
        linear_extrude(height=section_length * num_sections + (terminal ? tolerance / 2 : 0), center=true, convexity=3) {
            polygon(
                [
                    // Outer path.
                    [-corrected_side,                           corrected_side],
                    [-semi_sheet - semi_tolerance,              corrected_side],
                    [-semi_sheet - semi_tolerance,              semi_side - sheet_indent - semi_tolerance],
                    [semi_sheet + semi_tolerance,               semi_side - sheet_indent - semi_tolerance],
                    [semi_sheet + semi_tolerance,               semi_side],
                    [semi_side - inside_cut_side_length,        semi_side],
                    [semi_side,        semi_side - inside_cut_side_length],
                    [semi_side,                                 semi_sheet + semi_tolerance],
                    [semi_side - sheet_indent - semi_tolerance, semi_sheet + semi_tolerance],
                    [semi_side - sheet_indent - semi_tolerance, -semi_sheet - semi_tolerance],
                    [corrected_side,                            -semi_sheet - semi_tolerance],
                    [corrected_side,                            -corrected_side],
                    [-corrected_side + corrected_cut,           -corrected_side],
                    [-corrected_side,                           -corrected_side + corrected_cut],
            
                    // Inner path for stems.
                    [-semi_stem - semi_tolerance, semi_stem + semi_tolerance],
                    [semi_stem + semi_tolerance,  semi_stem + semi_tolerance],
                    [+semi_stem + semi_tolerance, -semi_stem - semi_tolerance],
                    [-semi_stem - semi_tolerance, -semi_stem - semi_tolerance],
                ],
                paths=[[for(i=[0:13]) i], [for(i=[14:17]) i]]
            );
        }
        
        // Screw holes.
        if(terminal && !skip_holes) {
            offset = ((num_sections - 1) * section_length) / 2;
            
            translate([-semi_side, semi_cut, -offset])
            rotate([0, 90, 0])
            screw_hole(
                screw_diameter=screw_diameter,
                screw_length=screw_length,
                head_diameter=head_diameter,
                screw_tolerance=screw_tolerance
            );
            
            translate([semi_cut, -semi_side, -offset])
            rotate([-90, 0, 0])
            screw_hole(
                screw_diameter=screw_diameter,
                screw_length=screw_length,
                head_diameter=head_diameter,
                screw_tolerance=screw_tolerance
            );
        }
    }
    
    // Stem stop plug.
    if(terminal && !skip_holes) {
        translate([0, 0, -(section_length * num_sections - stem_stop_depth - stem_length) / 2])
        cube([stem_side_length + 2 * tolerance, stem_side_length + 2 * tolerance, stem_stop_depth - tolerance], center=true);
    }
}


module linear_run3(
    num_sections=12,
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_length=screw_length,
    head_diameter=head_diameter,
    screw_tolerance=screw_tolerance,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
    brace_thickness=brace_thickness,
    terminal=true,
) {
    semi_side = side_length / 2;
    corrected_side = semi_side - (terminal ? brace_thickness + tolerance : 0);
    semi_sheet = sheet_width / 2;
    semi_tolerance = tolerance / 2;
    screw_semi_tolerance = screw_tolerance / 2;
    semi_stem = stem_side_length / 2;
    corrected_cut = max(0, cut_side_length - sin(45) * (terminal ? brace_thickness + tolerance : 0));
    semi_cut = corrected_cut / 2;
    
    // Main body and holes.
    difference() {
        // Profile extrusion.
        linear_extrude(height=section_length * num_sections + (terminal ? tolerance / 2 : 0), center=true, convexity=3) {
            polygon(
                [
                    // Outer path.
                    [-corrected_side + corrected_cut,           corrected_side],
                    [-semi_sheet - semi_tolerance,              corrected_side],
                    [-semi_sheet - semi_tolerance,              semi_side - sheet_indent - semi_tolerance],
                    [semi_sheet + semi_tolerance,               semi_side - sheet_indent - semi_tolerance],
                    [semi_sheet + semi_tolerance,               semi_side],
                    [semi_side - inside_cut_side_length,        semi_side],
                    [semi_side,                                 semi_side - inside_cut_side_length],
                    [semi_side,                                 semi_sheet + semi_tolerance],
                    [semi_side - sheet_indent - semi_tolerance, semi_sheet + semi_tolerance],
                    [semi_side - sheet_indent - semi_tolerance, -semi_sheet - semi_tolerance],
                    [semi_side,                                 -semi_sheet - semi_tolerance],
                    [semi_side,                                 -semi_side + inside_cut_side_length],
                    [semi_side - inside_cut_side_length,        -semi_side],
                    [semi_sheet + semi_tolerance,               -semi_side],
                    [semi_sheet + semi_tolerance,               -semi_side + sheet_indent + semi_tolerance],
                    [-semi_sheet - semi_tolerance,              -semi_side + sheet_indent + semi_tolerance],
                    [-semi_sheet - semi_tolerance,              -corrected_side],
                    [-corrected_side + corrected_cut,           -corrected_side],
                    [-corrected_side,                           -corrected_side + corrected_cut],
                    [-corrected_side,                           corrected_side - corrected_cut],
            
                    // Inner path for stems.
                    [-semi_stem - semi_tolerance, semi_stem + semi_tolerance],
                    [semi_stem + semi_tolerance,  semi_stem + semi_tolerance],
                    [+semi_stem + semi_tolerance, -semi_stem - semi_tolerance],
                    [-semi_stem - semi_tolerance, -semi_stem - semi_tolerance],
                ],
                paths=[[for(i=[0:19]) i], [for(i=[20:23]) i]]
            );
        }
        
        // Screw holes.
        if(terminal) {
            offset = ((num_sections - 1) * section_length) / 2;
            
            translate([-semi_side, 0, -offset])
            rotate([0, 90, 0])
            screw_hole(
                screw_diameter=screw_diameter,
                screw_length=screw_length,
                head_diameter=head_diameter,
                screw_tolerance=screw_tolerance
            );
        }
    }
    
    // Stem stop plug.
    if(terminal) {
        translate([0, 0, -(section_length * num_sections - stem_stop_depth - stem_length) / 2])
        cube([stem_side_length + 2 * tolerance, stem_side_length + 2 * tolerance, stem_stop_depth - tolerance], center=true);
    }
}


module brace2(
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_length=screw_length,
    screw_tolerance=screw_tolerance,
    head_diameter=head_diameter,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    thickness=brace_thickness,

    // Hinge option.
    hinge=false,
    side_knuckle_width=side_knuckle_width,
    num_knuckles=num_knuckles,
    hinge_tolerance=hinge_tolerance,
    hinge_wall_allowance=hinge_wall_allowance,
    nut_flat_width=nut_flat_width,
    nut_depth=nut_depth,
    nut_holder_wall_width=nut_holder_wall_width,

    // Door stop option.
    stop=false,
    magnet_diameter=magnet_diameter,
    magnet_depth=magnet_depth,
    magnet_holder_wall_width=magnet_holder_wall_width,
    
    // Door frame option (0, 1, 2).
    door_frame=door_frame_walls,
    
    // Snap-fit option.
    snap_fit=snap_fit_braces,
    door_snap_fit=snap_fit_doors,
    snap_fit_peg_tolerances=snap_fit_peg_tolerances,
) {
    semi_side = side_length / 2;
    semi_sheet = sheet_width / 2;
    semi_tolerance = tolerance / 2;
    corrected_cut = max(0, cut_side_length - sin(45) * (thickness + tolerance));
    semi_cut = corrected_cut / 2;    
    actual_length=2 * (section_length - tolerance);
    knuckle_width = (actual_length - 2 * side_knuckle_width) / num_knuckles;
    hinge_semi_tolerance = hinge_tolerance / 2;
    hinge_radius = thickness * sqrt(2) - hinge_semi_tolerance - hinge_wall_allowance / sqrt(2);
    pin_diameter = hinge_radius;
    stop_width = magnet_diameter + 2 * magnet_holder_wall_width + 2 * magnet_depth;
    stop_offset = thickness + tolerance + sheet_width;
    stop_depth = min(thickness, semi_side - semi_tolerance - semi_sheet - stop_offset);
    total_frame_width = 3 * thickness + hinge_tolerance;
    total_stop_width = thickness + hinge_tolerance + stop_width;    
    nut_radius = nut_flat_width / (2 * cos(30));
    
    difference() {
        // Profile extrusion.
        linear_extrude(height=actual_length, center=true, convexity=2) {
            polygon(
                [
                    // Outer path.
                    [-semi_side + thickness,                    semi_side - thickness],
                    [-semi_sheet - semi_tolerance,              semi_side - thickness],
                    [-semi_sheet - semi_tolerance,              semi_side],
                    [-semi_side,                                semi_side],
                    [-semi_side,                               -semi_side + cut_side_length],
                    [-semi_side + cut_side_length,  -semi_side],
                    [semi_side,                                 -semi_side],
                    [semi_side,                                 -semi_sheet - semi_tolerance],
                    [semi_side - thickness,                     -semi_sheet - semi_tolerance],
                    [semi_side - thickness,                     -semi_side + thickness],            
                    [-semi_side + thickness + tolerance + max(0,  corrected_cut - tolerance * sin(22.5)),  -semi_side + thickness],
                    [-semi_side + thickness,                    -semi_side + thickness + tolerance + max(0, corrected_cut - tolerance * sin(22.5))],
                ]
            );
        }
        
        if(!snap_fit) {
            // Screw holes.
            for(i=[-1:2:1]) {
                offset = (-section_length * i) / 2;
                
                translate([-semi_side, semi_cut, - offset])
                rotate([0, 90, 0])
                screw_hole(
                    screw_diameter=screw_diameter,
                    screw_length=screw_length,
                    head_diameter=head_diameter,
                    screw_tolerance=screw_tolerance
                );
                
                translate([semi_cut, -semi_side, - offset])
                rotate([-90, 0, 0])
                screw_hole(
                    screw_diameter=screw_diameter,
                    screw_length=screw_length,
                    head_diameter=head_diameter,
                    screw_tolerance=screw_tolerance
                );
            }              
        } 
        
        // Make space for rotating parts if we have a hinge.
        if(hinge) {
            translate([semi_side + hinge_semi_tolerance, -semi_side - hinge_semi_tolerance, 0]) {
                // Rotating allowance holes for odd (pinless) knuckles.
                for(i=[0:2:num_knuckles]) {
                    translate([0, 0, (i * 2 - num_knuckles + 1) / 2 * knuckle_width])
                    cylinder(h = knuckle_width + hinge_tolerance, r=hinge_radius + hinge_semi_tolerance, center=true);                        
                }                
            }
        }        
    }

    if(snap_fit) {
        // Snap fit pegs.
        for(i=[-1:2:1]) {
            offset = (-section_length * i) / 2;
            
            translate([-semi_side + thickness, semi_cut, - offset])
            rotate([0, 90, 0])
            snap_fit_peg(
                screw_diameter=screw_diameter,
                screw_tolerance=screw_tolerance,
                tolerance=tolerance,
                snap_fit_peg_tolerances=snap_fit_peg_tolerances
            );
            
            translate([semi_cut, -semi_side + thickness, - offset])
            rotate([-90, 0, 0])
            snap_fit_peg(
                screw_diameter=screw_diameter,
                screw_tolerance=screw_tolerance,
                tolerance=tolerance,
                snap_fit_peg_tolerances=snap_fit_peg_tolerances
            );
        }              
    } 
    
    if(hinge) {
        assert(
            side_knuckle_width <= knuckle_width,
            str("The side knuckles must be at most as big as the normal knuckles: ",  side_knuckle_width, " vs. ", knuckle_width)
        );
        
        // Fixed side.
        translate([semi_side + hinge_semi_tolerance, -semi_side - hinge_semi_tolerance, 0]) {           
            // Pin.
            cylinder(h=actual_length, r=pin_diameter / 2 - hinge_semi_tolerance, center=true);
            
            // Even (pinful) knuckles.
            for(i=[-1:2:num_knuckles]) {
                intersection() {
                    translate([0, 0, (i * 2 - num_knuckles + 1) / 2 * knuckle_width])
                    cylinder(h = knuckle_width - hinge_tolerance, r=hinge_radius - hinge_semi_tolerance, center=true);
                                            cylinder(h=actual_length, r=hinge_radius + hinge_semi_tolerance, center=true);
                }
            }
        }
        
        // Swiveling side.
        translate([semi_side + hinge_semi_tolerance, -semi_side - hinge_semi_tolerance, 0]) rotate([0, 0, -90]) {
            // Body.
            difference() {
                // Profile extrusion.
                linear_extrude(height=actual_length, center=true, convexity=2) {
                    polygon(
                        [
                            // Outer path.
                            [hinge_semi_tolerance, hinge_semi_tolerance],
                            [hinge_semi_tolerance + side_length - cut_side_length, hinge_semi_tolerance],
                            [hinge_semi_tolerance + side_length - cut_side_length + thickness, hinge_semi_tolerance + thickness],
                            [hinge_semi_tolerance + thickness, hinge_semi_tolerance + thickness],
                            [hinge_semi_tolerance + thickness, hinge_semi_tolerance + stop_offset],
                            [hinge_semi_tolerance + side_length - cut_side_length + thickness, hinge_semi_tolerance + stop_offset],
                            [hinge_semi_tolerance + side_length - cut_side_length + thickness, hinge_semi_tolerance + stop_offset + stop_depth],
                            [hinge_semi_tolerance, hinge_semi_tolerance + stop_offset + stop_depth],
                        ]
                    );
                }
                                
                // Rotating allowance holes for even (pinful) knuckles.
                for(i=[-1:2:num_knuckles]) {
                    translate([0, 0, (i * 2 - num_knuckles + 1) / 2 * knuckle_width])
                    cylinder(h = knuckle_width + hinge_tolerance, r=hinge_radius + hinge_semi_tolerance, center=true);                    
                }                    

                // Rotating allowance holes for the pin in odd (pinless) knuckles.
                for(i=[0:2:num_knuckles]) {
                    translate([0, 0, (i * 2 - num_knuckles + 1) / 2 * knuckle_width])
                    cylinder(h = knuckle_width + hinge_tolerance, r=pin_diameter / 2 + hinge_semi_tolerance, center=true);
                }              
          
                if(!door_snap_fit) {
                    // Screw holes.
                    for(i=[-1:2:1]) {
                        offset = (-section_length * i) / 2;
                      
                        // Front hole.
                        translate([semi_side - semi_cut + hinge_semi_tolerance, hinge_semi_tolerance, - offset])
                        rotate([-90, 0, 0])
                        screw_hole(
                            screw_diameter=screw_diameter,
                            screw_length=semi_side,
                            head_diameter=head_diameter,
                            screw_tolerance=screw_tolerance
                        );
                    }
                } 
            }

            for(i=[-1:2:1]) {
                offset = (-section_length * i) / 2;
                
                if(door_snap_fit) {
                    // Snap fit pegs.
                    translate([semi_side - semi_cut + hinge_semi_tolerance, hinge_semi_tolerance + thickness + sheet_width + tolerance, - offset])
                    rotate([90, 0, 0])
                    snap_fit_peg(
                        screw_diameter=screw_diameter,
                        screw_tolerance=screw_tolerance,
                        tolerance=tolerance,
                        snap_fit_peg_tolerances=snap_fit_peg_tolerances
                    );

                    translate([semi_side - semi_cut + hinge_semi_tolerance, hinge_semi_tolerance + thickness, - offset])
                    rotate([-90, 0, 0])
                    snap_fit_peg(
                        screw_diameter=screw_diameter,
                        screw_tolerance=screw_tolerance,
                        tolerance=tolerance,
                        snap_fit_peg_tolerances=snap_fit_peg_tolerances
                    );
                }
                else {
                    // Nut holders.
                    translate([semi_side - semi_cut + hinge_semi_tolerance, hinge_semi_tolerance + stop_offset + stop_depth + nut_depth / 2, - offset])
                    rotate([-90, 30, 0])
                    difference() {
                        cylinder(h=nut_depth + tolerance, r1=nut_radius + semi_tolerance + nut_holder_wall_width + nut_depth, r2=nut_radius + semi_tolerance + nut_holder_wall_width, $fn=6, center=true);     
                        cylinder(h=nut_depth + 2 * tolerance, r=nut_radius + semi_tolerance, $fn=6, center=true);     
                    }
                }
            }              

            // Knuckles.
            for(i=[0:2:num_knuckles]) {
                intersection() {
                    translate([0, 0, (i * 2 - num_knuckles + 1) / 2 * knuckle_width])
                    difference() {
                        // Knuckle body.
                        cylinder(h = knuckle_width - hinge_tolerance, r=hinge_radius - hinge_semi_tolerance, center=true);
                        // Hole for the pin.
                        cylinder(h = knuckle_width, r=pin_diameter / 2 + hinge_semi_tolerance, center=true);
                    }
                    cylinder(h=actual_length, r=hinge_radius + hinge_semi_tolerance, center=true);
                }
            }                
        }
    }
    
    if(door_frame > 1) {
        // Frame segment.
        translate([semi_side + total_frame_width / 2 - thickness, -semi_side + stop_offset + stop_depth / 2, 0])
        cube([total_frame_width, stop_depth, actual_length], center=true);                         
    }
    if(door_frame > 0) {
        // Frame segment.
        translate([-semi_side + stop_offset + stop_depth / 2, semi_side + total_frame_width / 2 - thickness, 0])
        cube([stop_depth, total_frame_width, actual_length], center=true);                         
    }
    
    if(stop) {
        // Main stop segment.
        difference() {
            translate([semi_side + total_stop_width / 2 - thickness, -semi_side + stop_offset + stop_depth / 2, 0])
            cube([total_stop_width, stop_depth, actual_length], center=true);
                        
            // Cut corners.
            translate([semi_side + total_stop_width - thickness, -semi_side + stop_offset + stop_depth / 2, actual_length / 2])
            rotate([0, 45, 0])
            cube([cut_side_length * sqrt(2), stop_depth + tolerance, cut_side_length * sqrt(2)], center=true);

            translate([semi_side + total_stop_width - thickness, -semi_side + stop_offset + stop_depth / 2, -actual_length / 2])
            rotate([0, 45, 0])
            cube([cut_side_length * sqrt(2), stop_depth + tolerance, cut_side_length * sqrt(2)], center=true);
        }
        
        // Magnet holders.
        for(i=[-1:2:1]) {
            offset = (-section_length * i) / 2;                        
            translate([semi_side + hinge_tolerance + stop_width / 2, -semi_side + stop_offset + stop_depth + magnet_depth / 2- semi_tolerance, - offset])
            rotate([-90, 0, 0])
            difference() {
                cylinder(h=magnet_depth + tolerance, r1=magnet_diameter / 2 + magnet_holder_wall_width + magnet_depth, r2=magnet_diameter / 2 + magnet_holder_wall_width, center=true);     
                cylinder(h=magnet_depth + 2 * tolerance, r=magnet_diameter / 2 + tolerance, center=true);     
            }
        }               
    }
}


module brace3(
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_length=screw_length,
    screw_tolerance=screw_tolerance,
    head_diameter=head_diameter,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    thickness=brace_thickness,
    
    // Snap-fit option.
    snap_fit=snap_fit_braces,
) {
    semi_side = side_length / 2;
    semi_sheet = sheet_width / 2;
    semi_tolerance = tolerance / 2;
    corrected_cut = max(0, cut_side_length - sin(45) * (thickness + tolerance));
    semi_cut = corrected_cut / 2;    
    actual_length=2 * (section_length - tolerance);
    knuckle_width = (actual_length - 2 * side_knuckle_width) / num_knuckles;
    hinge_semi_tolerance = hinge_tolerance / 2;
    hinge_radius = thickness * sqrt(2) - hinge_semi_tolerance - hinge_wall_allowance / sqrt(2);
    pin_diameter = hinge_radius;
    stop_width = magnet_diameter + 2 * magnet_holder_wall_width + 2 * magnet_depth;
    stop_depth = semi_side - semi_tolerance - semi_sheet - thickness - tolerance - sheet_width;
    total_stop_width = thickness + tolerance + stop_width;    
    nut_radius = sqrt(1 + cos(30)) * nut_flat_width / 2;
    
    difference() {
        // Profile extrusion.
        linear_extrude(height=actual_length, center=true, convexity=2) {
            polygon(
                [
                    // Outer path.            
                    [-semi_sheet - semi_tolerance,              semi_side - thickness],
                    [-semi_sheet - semi_tolerance,              semi_side],            
                    [-semi_side + cut_side_length,              semi_side],
                    [-semi_side,                                semi_side - cut_side_length],
                    [-semi_side,                                -semi_side + cut_side_length],
                    [-semi_side + cut_side_length,              -semi_side],
                    [-semi_sheet - semi_tolerance,              -semi_side],            
                    [-semi_sheet - semi_tolerance,              -semi_side + thickness],            
                    [-semi_side + thickness + tolerance + max(0,  corrected_cut - tolerance * sin(22.5)),  -semi_side + thickness],
                    [-semi_side + thickness,                    -semi_side + thickness + tolerance + max(0, corrected_cut - tolerance * sin(22.5))],
                    [-semi_side + thickness,                    semi_side - thickness - tolerance - max(0, corrected_cut - tolerance * sin(22.5))],
                    [-semi_side + thickness + tolerance + max(0,  corrected_cut - tolerance * sin(22.5)),  semi_side - thickness],
            
                ]
            );
        }
        
        if(!snap_fit) {
            // Screw holes.
            for(i=[-1:2:1]) {
                offset = (-section_length * i) / 2;
                
                translate([-semi_side, 0, - offset])
                rotate([0, 90, 0])
                screw_hole(
                    screw_diameter=screw_diameter,
                    screw_length=screw_length,
                    head_diameter=head_diameter,
                    screw_tolerance=screw_tolerance
                );
            }              
        } 
    }

    if(snap_fit) {
        // Snap fit pegs.
        peg_length = screw_length - brace_thickness - 2 * tolerance;
        
        for(i=[-1:2:1]) {
            offset = (-section_length * i) / 2;
            
            translate([-semi_side + thickness, 0, - offset])
            rotate([0, 90, 0])
            snap_fit_peg(
                screw_diameter=screw_diameter,
                screw_tolerance=screw_tolerance,
                tolerance=tolerance,
                snap_fit_peg_tolerances=peg_length / tolerance
            );
        }              
    } 
}


module stem(
    tolerance=tolerance,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
) {
    cube([stem_side_length - tolerance, stem_side_length - tolerance, stem_length - tolerance], center=true);
}


module insert_holes(
    num_sections=num_sections,
    section_length=section_length,
    side_length=side_length,
    insert_thickness=insert_thickness,
    insert_length=insert_length,
    tolerance=tolerance,
    insert_slots=insert_slots,
) {
    diagonal = sqrt(side_length * side_length / 2);
    
    module insert_hole() {
        rotate([0, 0, 45])
        translate([diagonal / 2, 0, 0])
        cube([diagonal + tolerance, insert_thickness + tolerance, insert_length + tolerance], center=true);        
    }    
    
    // Insert holes.
    if(num_sections >= 4 && insert_slots) {
        translate([0, 0, (num_sections - 2.5) / 2 * section_length])
        insert_hole();
        translate([0, 0, (num_sections - 3.5) / 2 * section_length])
        insert_hole();
        translate([0, 0, -(num_sections - 2.5) / 2 * section_length])
        insert_hole();
        translate([0, 0, -(num_sections - 3.5) / 2 * section_length])
        insert_hole();
    }    
}


module cable_tie_conduits(
    num_sections=num_sections,
    section_length=section_length,
    side_length=side_length,
    inside_cut_side_length=inside_cut_side_length,
    sheet_width=sheet_width,
    stem_side_length=stem_side_length,
    tolerance=tolerance,
    cable_tie_conduits=cable_tie_conduits,
    cable_tie_hole_width=cable_tie_hole_width,
    cable_tie_hole_height=cable_tie_hole_height,
    cable_tie_holder_diameter=cable_tie_holder_diameter,
    inside_only=true,
) {
    module conduit() {
        outer_radius = side_length / 2 - sheet_width / 2 - tolerance / 2;
        
        module inside_conduit(grow=0) {
            difference() {
                cylinder(h=cable_tie_hole_width + grow, r=outer_radius + tolerance / 2 + grow, center=true);
                cylinder(h=cable_tie_hole_width + grow + tolerance, r=outer_radius - cable_tie_hole_height - tolerance / 2 - grow, center=true);
            }         
        }
        
        translate([side_length / 2, side_length / 2, 0])
        if(inside_only) {
            inside_conduit();
        } else {
            difference() {
                intersection() {
                    inside_conduit(grow=cable_tie_holder_diameter);
                    union() {
                        translate([-outer_radius / 2, -outer_radius / 2, 0])
                        cube([outer_radius, outer_radius, cable_tie_hole_width + cable_tie_holder_diameter], center=true);
                        translate([-side_length / 2, -side_length / 2, 0])
                        cube([stem_side_length + tolerance, stem_side_length + tolerance, cable_tie_hole_width + cable_tie_holder_diameter], center=true);
                    }
                    rotate([0, 0, 45])
                    translate([-side_length - sqrt(inside_cut_side_length * inside_cut_side_length * 2) / 2, 0, 0])
                    cube([2 * side_length, 2 * side_length, cable_tie_hole_width + cable_tie_holder_diameter], center=true);
                }
                
                inside_conduit();
            }
        }
    }
    
    if(cable_tie_conduits) {
        translate([0, 0, (num_sections - 3) / 2 * section_length])
        conduit();
        translate([0, 0, -(num_sections - 3) / 2 * section_length])
        conduit();        
    }
}


module edge2(
    num_sections=num_sections,
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_tolerance=screw_tolerance,
    screw_length=screw_length,
    head_diameter=head_diameter,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
    brace_thickness=brace_thickness,
    door_frame=door_frame_walls,
    cable_hole_cables=cable_hole_cables,
    cable_hole_diameter=cable_hole_diameter,
    cable_hole_cover_width=cable_hole_cover_width,
    cable_hole_fractional_position=cable_hole_fractional_position,
    insert_slots=insert_slots,    
    insert_thickness=insert_thickness,
    insert_length=insert_length,    
    cable_tie_conduits=cable_tie_conduits,
    cable_tie_hole_width=cable_tie_hole_width,
    cable_tie_hole_height=cable_tie_hole_height,    
    cable_tie_holder_diameter=cable_tie_holder_diameter,
) {
    module without_holes() {
        union() {
            // The center module.
            difference() {
                linear_run2(
                    num_sections=num_sections - 2,
                    section_length=section_length,
                    screw_diameter=screw_diameter,
                    head_diameter=head_diameter,
                    screw_tolerance=screw_tolerance,
                    tolerance=tolerance,
                    sheet_width=sheet_width,
                    sheet_indent=sheet_indent,
                    side_length=side_length,
                    cut_side_length=cut_side_length,
                    inside_cut_side_length=inside_cut_side_length,
                    stem_side_length=stem_side_length,
                    stem_length=stem_length,
                    stem_stop_depth=stem_stop_depth,
                    screw_length=screw_length,
                    brace_thickness=brace_thickness,
                    terminal=false
                );
                
            }
            
            // The two ends.
            translate([0, 0, -(num_sections - 1) * section_length / 2])
            linear_run2(
                num_sections=1,
                section_length=section_length,
                screw_diameter=screw_diameter,
                head_diameter=head_diameter,
                screw_tolerance=screw_tolerance,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                screw_length=screw_length,
                brace_thickness=brace_thickness,
                terminal=true
            );            
            translate([0, 0, (num_sections - 1) * section_length / 2])
            rotate([0, 180, -90])
            linear_run2(
                num_sections=1,
                section_length=section_length,
                screw_diameter=screw_diameter,
                head_diameter=head_diameter,
                screw_tolerance=screw_tolerance,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                screw_length=screw_length,
                brace_thickness=brace_thickness,
                terminal=true
            );            
            
            // Door frames
            {
                thickness = brace_thickness;
                semi_side = side_length / 2;
                semi_sheet = sheet_width / 2;
                semi_tolerance = tolerance / 2;
                stop_offset = thickness + tolerance + sheet_width;
                stop_depth = min(thickness, semi_side - semi_tolerance - semi_sheet - stop_offset);
                total_frame_width = 3 * thickness + hinge_tolerance;
                actual_length = (num_sections - 2) * section_length;
                if(door_frame > 1) {        
                    // Frame segment.
                    translate([semi_side + total_frame_width / 2 - thickness, -semi_side + stop_offset + stop_depth / 2, 0])
                    cube([total_frame_width, stop_depth, actual_length], center=true);   
                }
                if(door_frame > 0) {        
                    // Frame segment.
                    translate([-semi_side + stop_offset + stop_depth / 2, semi_side + total_frame_width / 2 - thickness, 0])
                    cube([stop_depth, total_frame_width, actual_length], center=true);   
                }
            }

            // Outer cable tie conduits.
            cable_tie_conduits(
                num_sections=num_sections,
                section_length=section_length,
                side_length=side_length,
                inside_cut_side_length=inside_cut_side_length,
                sheet_width=sheet_width,
                stem_side_length=stem_side_length,
                tolerance=tolerance,
                cable_tie_conduits=cable_tie_conduits,
                cable_tie_hole_width=cable_tie_hole_width,
                cable_tie_hole_height=cable_tie_hole_height,
                cable_tie_holder_diameter=cable_tie_holder_diameter,
                inside_only=false
            );
        }
    }
    
    module holes(r_plus=0) {        
        // Cable holes if needed.
        displacement = section_length * (num_sections - 2) * (0.5 - cable_hole_fractional_position);
        translate([0, 0, displacement])
        if(cable_hole_cables > 0) {
            assert(
                abs(displacement) + cable_hole_cover_width + cable_hole_cables * cable_hole_diameter / 2 <= section_length * (num_sections - 2) / 2,
                str("cable_hole_fractional_position is too much off-center or the cable holes don't fit")
            );            
            
            
            hull() {
                translate([0, -side_length / 2 + cable_hole_diameter / 2, (cable_hole_cables - 1) * cable_hole_diameter / 2])
                rotate([0, 90, 0])
                cylinder(h=side_length + brace_thickness * 6 + tolerance, r=cable_hole_diameter / 2 + tolerance / 2 + r_plus, center=true);
                
                translate([0, -side_length / 2 + cable_hole_diameter / 2, -(cable_hole_cables - 1) * cable_hole_diameter / 2])
                rotate([0, 90, 0])
                cylinder(h=side_length + brace_thickness * 6 + tolerance, r=cable_hole_diameter / 2 + tolerance / 2 + r_plus, center=true);  
            }          
        }
    }
    
    difference() {
        union() {
            without_holes();
            
            if(cable_hole_cables > 0) {
                intersection() {
                    hull() {
                        without_holes();
                    }
                    holes(r_plus=cable_hole_cover_width);
                }
            }            
        }
        
        // Shave off half a tolerance on farther ends.
        translate([0, 0, section_length * num_sections / 2])
        cube([side_length + tolerance, side_length + tolerance, tolerance], center=true);

        translate([0, 0, -section_length * num_sections / 2])
        cube([side_length + tolerance, side_length + tolerance, tolerance], center=true);
        
        // Cable holes if needed.
        holes();
        
        // Insert holes.
        insert_holes(
            num_sections=num_sections,
            section_length=section_length,
            side_length=side_length,
            insert_thickness=insert_thickness,
            insert_length=insert_length,
            tolerance=tolerance,
            insert_slots=insert_slots
        );
        
        // Inner cable tie conduits.
        cable_tie_conduits(
            num_sections=num_sections,
            section_length=section_length,
            side_length=side_length,
            inside_cut_side_length=inside_cut_side_length,
            sheet_width=sheet_width,
            stem_side_length=stem_side_length,
            tolerance=tolerance,
            cable_tie_conduits=cable_tie_conduits,
            cable_tie_hole_width=cable_tie_hole_width,
            cable_tie_hole_height=cable_tie_hole_height,
            cable_tie_holder_diameter=cable_tie_holder_diameter,
            inside_only=true
        );        
    }
}


module edge3(
    num_sections=num_sections,
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_tolerance=screw_tolerance,
    screw_length=screw_length,
    head_diameter=head_diameter,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
    brace_thickness=brace_thickness,
    insert_slots=insert_slots,
    insert_thickness=insert_thickness,
    insert_length=insert_length,
    cable_tie_conduits=cable_tie_conduits,
    cable_tie_hole_width=cable_tie_hole_width,
    cable_tie_hole_height=cable_tie_hole_height,
    cable_tie_holder_diameter=cable_tie_holder_diameter,
) {
    difference() {
        union() {
            // The center module.
            linear_run3(
                num_sections=num_sections - 2,
                section_length=section_length,
                screw_diameter=screw_diameter,
                head_diameter=head_diameter,
                screw_tolerance=screw_tolerance,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                screw_length=screw_length,
                brace_thickness=brace_thickness,
                terminal=false
            );
            
            // The two ends.
            translate([0, 0, -(num_sections - 1) * section_length / 2])
            linear_run3(
                num_sections=1,
                section_length=section_length,
                screw_diameter=screw_diameter,
                head_diameter=head_diameter,
                screw_tolerance=screw_tolerance,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                screw_length=screw_length,
                brace_thickness=brace_thickness,
                terminal=true
            );            
            translate([0, 0, (num_sections - 1) * section_length / 2])
            rotate([0, 180, 180])
            linear_run3(
                num_sections=1,
                section_length=section_length,
                screw_diameter=screw_diameter,
                head_diameter=head_diameter,
                screw_tolerance=screw_tolerance,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                screw_length=screw_length,
                brace_thickness=brace_thickness,
                terminal=true
            );            
            
            // Outer cable tie conduits.
            for(i=[-90:90:0]) {
                rotate([0, 0, i]) {
                    cable_tie_conduits(
                        num_sections=num_sections,
                        section_length=section_length,
                        side_length=side_length,
                        inside_cut_side_length=inside_cut_side_length,
                        sheet_width=sheet_width,
                        stem_side_length=stem_side_length,
                        tolerance=tolerance,
                        cable_tie_conduits=cable_tie_conduits,
                        cable_tie_hole_width=cable_tie_hole_width,
                        cable_tie_hole_height=cable_tie_hole_height,
                        cable_tie_holder_diameter=cable_tie_holder_diameter,
                        inside_only=false
                    );                  
                }
            }
        }
        
        // Insert holes.
        for(i=[-90:90:0]) {
            rotate([0, 0, i]) {
                insert_holes(
                    num_sections=num_sections,
                    section_length=section_length,
                    side_length=side_length,
                    insert_thickness=insert_thickness,
                    insert_length=insert_length,
                    tolerance=tolerance,
                    insert_slots=insert_slots
                );
                
                // Inner cable tie conduits.
                cable_tie_conduits(
                    num_sections=num_sections,
                    section_length=section_length,
                    side_length=side_length,
                    inside_cut_side_length=inside_cut_side_length,
                    sheet_width=sheet_width,
                    stem_side_length=stem_side_length,
                    tolerance=tolerance,
                    cable_tie_conduits=cable_tie_conduits,
                    cable_tie_hole_width=cable_tie_hole_width,
                    cable_tie_hole_height=cable_tie_hole_height,
                    cable_tie_holder_diameter=cable_tie_holder_diameter,
                    inside_only=true
                );
            }
        }
        
        // Shave off half a tolerance on farther ends.
        translate([0, 0, section_length * num_sections / 2])
        cube([side_length + tolerance, side_length + tolerance, tolerance], center=true);

        translate([0, 0, -section_length * num_sections / 2])
        cube([side_length + tolerance, side_length + tolerance, tolerance], center=true);
    }
}


module edge4(
    num_sections=num_sections,
    section_length=section_length,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
    insert_slots=insert_slots,
    insert_thickness=insert_thickness,
    insert_length=insert_length,
    cable_tie_conduits=cable_tie_conduits,
    cable_tie_hole_width=cable_tie_hole_width,
    cable_tie_hole_height=cable_tie_hole_height,
    cable_tie_holder_diameter=cable_tie_holder_diameter,
) {
    semi_side = side_length / 2;
    semi_sheet = sheet_width / 2;
    semi_tolerance = tolerance / 2;
    semi_stem = stem_side_length / 2;
    
    // Main body and holes.
    difference() {
        union() {
            // Profile extrusion.
            linear_extrude(height=section_length * num_sections - tolerance, center=true, convexity=3) {
                polygon(
                    [
                        // Outer path.
                        [-semi_side,                                 semi_side - inside_cut_side_length],
                        [-semi_side + inside_cut_side_length,        semi_side],
                        [-semi_sheet - semi_tolerance,               semi_side],
                        [-semi_sheet - semi_tolerance,               semi_side - sheet_indent - semi_tolerance],
                        [semi_sheet + semi_tolerance,                semi_side - sheet_indent - semi_tolerance],
                        [semi_sheet + semi_tolerance,                semi_side],
                        [semi_side - inside_cut_side_length,         semi_side],
                        [semi_side,                                  semi_side - inside_cut_side_length],
                        [semi_side,                                  semi_sheet + semi_tolerance],
                        [semi_side - sheet_indent - semi_tolerance,  semi_sheet + semi_tolerance],
                        [semi_side - sheet_indent - semi_tolerance,  -semi_sheet - semi_tolerance],
                        [semi_side,                                  -semi_sheet - semi_tolerance],
                        [semi_side,                                  -semi_side + inside_cut_side_length],
                        [semi_side - inside_cut_side_length,         -semi_side],
                        [semi_sheet + semi_tolerance,                -semi_side],
                        [semi_sheet + semi_tolerance,                -semi_side + sheet_indent + semi_tolerance],
                        [-semi_sheet - semi_tolerance,               -semi_side + sheet_indent + semi_tolerance],
                        [-semi_sheet - semi_tolerance,               -semi_side],
                        [-semi_side + inside_cut_side_length,        -semi_side],
                        [-semi_side,                                 -semi_side + inside_cut_side_length],
                        [-semi_side,                                 -semi_sheet - semi_tolerance],
                        [-semi_side + sheet_indent + semi_tolerance, -semi_sheet - semi_tolerance],
                        [-semi_side + sheet_indent + semi_tolerance, semi_sheet + semi_tolerance],
                        [-semi_side,                                 semi_sheet + semi_tolerance],
                
                        // Inner path for stems.
                        [-semi_stem - semi_tolerance, semi_stem + semi_tolerance],
                        [semi_stem + semi_tolerance,  semi_stem + semi_tolerance],
                        [+semi_stem + semi_tolerance, -semi_stem - semi_tolerance],
                        [-semi_stem - semi_tolerance, -semi_stem - semi_tolerance],
                    ],
                    paths=[[for(i=[0:23]) i], [for(i=[24:27]) i]]
                );
            }
            
            // Outer cable tie conduits.
            for(i=[0:90:270]) {
                rotate([0, 0, i]) {
                    cable_tie_conduits(
                        num_sections=num_sections,
                        section_length=section_length,
                        side_length=side_length,
                        inside_cut_side_length=inside_cut_side_length,
                        sheet_width=sheet_width,
                        stem_side_length=stem_side_length,
                        tolerance=tolerance,
                        cable_tie_conduits=cable_tie_conduits,
                        cable_tie_hole_width=cable_tie_hole_width,
                        cable_tie_hole_height=cable_tie_hole_height,
                        cable_tie_holder_diameter=cable_tie_holder_diameter,
                        inside_only=false
                    );                  
                }
            }            
        }
        
        // Insert holes.
        for(i=[0:90:270]) {
            rotate([0, 0, i]) {
                insert_holes(
                    num_sections=num_sections,
                    section_length=section_length,
                    side_length=side_length,
                    insert_thickness=insert_thickness,
                    insert_length=insert_length,
                    tolerance=tolerance,
                    insert_slots=insert_slots
                );
                
                // Inner cable tie conduits.
                cable_tie_conduits(
                    num_sections=num_sections,
                    section_length=section_length,
                    side_length=side_length,
                    inside_cut_side_length=inside_cut_side_length,
                    sheet_width=sheet_width,
                    stem_side_length=stem_side_length,
                    tolerance=tolerance,
                    cable_tie_conduits=cable_tie_conduits,
                    cable_tie_hole_width=cable_tie_hole_width,
                    cable_tie_hole_height=cable_tie_hole_height,
                    cable_tie_holder_diameter=cable_tie_holder_diameter,
                    inside_only=true
                );
            }
        }
    }
    
    // Stem stop plugs.
    for(a=[0:180:180]) {
        rotate([a, 0, 0])
        translate([0, 0, -(section_length * num_sections - stem_stop_depth - stem_length) / 2])
        cube([stem_side_length + 2 * tolerance, stem_side_length + 2 * tolerance, stem_stop_depth - tolerance], center=true);
    }
}


module connect222(
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_length=screw_length,
    screw_tolerance=screw_tolerance,
    head_diameter=head_diameter,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
    brace_thickness=brace_thickness,
) {
    semi_sheet = sheet_width / 2;
    semi_tolerance = tolerance / 2;
    semi_side = side_length / 2;
    
    difference() {
        union() {
            // Arms for the three axes.            
            translate([0, 0, section_length / 2 + semi_side])
            rotate([0, 180, -90])            
            linear_run2(
                num_sections=1,
                section_length=section_length,
                screw_diameter=screw_diameter,
                screw_tolerance=screw_tolerance,
                screw_length=screw_length,
                head_diameter=head_diameter,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                brace_thickness=brace_thickness,
                terminal=true
            );
            
            translate([0, section_length / 2 + semi_side, 0])
            rotate([-90, -90, 0])
            rotate([0, 180, -90])            
            linear_run2(
                num_sections=1,
                section_length=section_length,
                screw_diameter=screw_diameter,
                screw_tolerance=screw_tolerance,
                screw_length=screw_length,
                head_diameter=head_diameter,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                brace_thickness=brace_thickness,
                terminal=true
            );    
            
            translate([section_length / 2 + semi_side, 0, 0])
            rotate([-90, 180, -90])
            rotate([0, 180, -90])
            linear_run2(
                num_sections=1,
                section_length=section_length,
                screw_diameter=screw_diameter,
                screw_tolerance=screw_tolerance,
                screw_length=screw_length,
                head_diameter=head_diameter,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                brace_thickness=brace_thickness,
                terminal=true
            );        
             
            // Corner parts intersection.
            corner_sections = side_length / section_length;
            intersection() {
                linear_run2(
                    num_sections=corner_sections,
                    section_length=section_length,
                    screw_diameter=screw_diameter,
                    screw_tolerance=screw_tolerance,
                    screw_length=screw_length,
                    head_diameter=head_diameter,
                    tolerance=tolerance,
                    sheet_width=sheet_width,
                    sheet_indent=sheet_indent,
                    side_length=side_length,
                    cut_side_length=cut_side_length,
                    inside_cut_side_length=inside_cut_side_length,
                    stem_side_length=stem_side_length,
                    stem_length=stem_length,
                    stem_stop_depth=stem_stop_depth,
                    brace_thickness=brace_thickness,
                    terminal=false
                );
                
                rotate([90, 0, 0])
                linear_run2(
                    num_sections=corner_sections,
                    section_length=section_length,
                    screw_diameter=screw_diameter,
                    screw_tolerance=screw_tolerance,
                    screw_length=screw_length,
                    head_diameter=head_diameter,
                    tolerance=tolerance,
                    sheet_width=sheet_width,
                    sheet_indent=sheet_indent,
                    side_length=side_length,
                    cut_side_length=cut_side_length,
                    inside_cut_side_length=inside_cut_side_length,
                    stem_side_length=stem_side_length,
                    stem_length=stem_length,
                    stem_stop_depth=stem_stop_depth,
                    brace_thickness=brace_thickness,
                    terminal=false
                );    
                
                rotate([0, -90, 0])
                linear_run2(
                    num_sections=corner_sections,
                    section_length=section_length,
                    screw_diameter=screw_diameter,
                    screw_tolerance=screw_tolerance,
                    screw_length=screw_length,
                    head_diameter=head_diameter,
                    tolerance=tolerance,
                    sheet_width=sheet_width,
                    sheet_indent=sheet_indent,
                    side_length=side_length,
                    cut_side_length=cut_side_length,
                    inside_cut_side_length=inside_cut_side_length,
                    stem_side_length=stem_side_length,
                    stem_length=stem_length,
                    stem_stop_depth=stem_stop_depth,
                    brace_thickness=brace_thickness,
                    terminal=false
                );   
            }
            
            
            // Filling the outer holes caused by intersection.
            translate([cut_side_length / 2, cut_side_length / 2, -semi_side + (side_length - sheet_width - tolerance) / 4])
            cube([side_length - cut_side_length, side_length - cut_side_length, (side_length - sheet_width - tolerance) / 2], center=true);

            translate([-semi_side + (side_length - sheet_width - tolerance) / 4, cut_side_length / 2, cut_side_length / 2])
            cube([(side_length - sheet_width - tolerance) / 2, side_length - cut_side_length, side_length - cut_side_length], center=true);

            translate([cut_side_length / 2, -semi_side + (side_length - sheet_width - tolerance) / 4, cut_side_length / 2])
            cube([side_length - cut_side_length, (side_length - sheet_width - tolerance) / 2, side_length - cut_side_length], center=true);
            
            // Filling in the rail holes caused by intersection.
            translate([semi_side - (sheet_indent - tolerance / 2) / 2, 0, 0])
            rotate([0, -90, 0])
            linear_run2(num_sections=(sheet_indent + tolerance) / section_length, skip_holes=true);

            rotate([90, 0, 90])
            translate([semi_side - (sheet_indent - tolerance / 2) / 2, 0, 0])
            rotate([0, -90, 0])
            linear_run2(num_sections=(sheet_indent + tolerance) / section_length, skip_holes=true);

            rotate([0, -90, -90])
            translate([semi_side - (sheet_indent - tolerance / 2) / 2, 0, 0])
            rotate([0, -90, 0])
            linear_run2(num_sections=(sheet_indent + tolerance) / section_length, skip_holes=true);
        }
        
        // Cut out the corner support hexagon.
        modulus = norm([1, 1, 1]);
        b = acos(1 / modulus); // inclination angle
        c = atan2(1, 1);        // azimuthal angle
        rotate([0, b, c]) 
        translate([0, 0, - side_length - sqrt(3 / 4) * side_length + sqrt(3) * cut_side_length])
        cylinder(h=side_length, r=2*side_length);
        
        // Shave off half a tolerance on farther ends.
        translate([0, 0, section_length + semi_side])
        cube([side_length + tolerance, side_length + tolerance, tolerance], center=true);

        translate([0, section_length + semi_side, 0])
        cube([side_length + tolerance, tolerance, side_length + tolerance], center=true);

        translate([section_length + semi_side, 0, 0])
        cube([tolerance, side_length + tolerance, side_length + tolerance], center=true);


    }
}


module connect22(
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_tolerance=screw_tolerance,
    screw_length=screw_length,
    head_diameter=head_diameter,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
    brace_thickness=brace_thickness,
    door_frame=door_frame_walls,
) {
    edge2(
        num_sections=2 + side_length / section_length,
        section_length=section_length,
        screw_diameter=screw_diameter,
        screw_tolerance=screw_tolerance,
        screw_length=screw_length,
        head_diameter=head_diameter,
        tolerance=tolerance,
        sheet_width=sheet_width,
        sheet_indent=sheet_indent,
        side_length=side_length,
        cut_side_length=cut_side_length,
        inside_cut_side_length=inside_cut_side_length,
        stem_side_length=stem_side_length,
        stem_length=stem_length,
        stem_stop_depth=stem_stop_depth,
        brace_thickness=brace_thickness,
        door_frame=door_frame
    );
}

module connect33(
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_tolerance=screw_tolerance,
    screw_length=screw_length,
    head_diameter=head_diameter,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
    brace_thickness=brace_thickness,
) {
    edge3(
        num_sections=2 + side_length / section_length,
        section_length=section_length,
        screw_diameter=screw_diameter,
        screw_tolerance=screw_tolerance,
        screw_length=screw_length,
        head_diameter=head_diameter,
        tolerance=tolerance,
        sheet_width=sheet_width,
        sheet_indent=sheet_indent,
        side_length=side_length,
        cut_side_length=cut_side_length,
        inside_cut_side_length=inside_cut_side_length,
        stem_side_length=stem_side_length,
        stem_length=stem_length,
        stem_stop_depth=stem_stop_depth,
        brace_thickness=brace_thickness
    );
}


module connect44(
    section_length=section_length,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
) {
    edge4(
        num_sections=2 + side_length / section_length,
        section_length=section_length,
        tolerance=tolerance,
        sheet_width=sheet_width,
        sheet_indent=sheet_indent,
        side_length=side_length,
        inside_cut_side_length=inside_cut_side_length,
        stem_side_length=stem_side_length,
        stem_length=stem_length,
        stem_stop_depth=stem_stop_depth
    );
}


module connect2233(
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_tolerance=screw_tolerance,
    screw_length=screw_length,
    head_diameter=head_diameter,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
    brace_thickness=brace_thickness,
) {
    difference() {
        edge2(
            num_sections=2 + side_length / section_length,
            section_length=section_length,
            screw_diameter=screw_diameter,
            screw_tolerance=screw_tolerance,
            screw_length=screw_length,
            head_diameter=head_diameter,
            tolerance=tolerance,
            sheet_width=sheet_width,
            sheet_indent=sheet_indent,
            side_length=side_length,
            cut_side_length=cut_side_length,
            inside_cut_side_length=inside_cut_side_length,
            stem_side_length=stem_side_length,
            stem_length=stem_length,
            stem_stop_depth=stem_stop_depth,
            brace_thickness=brace_thickness
        );            
        
        translate([side_length / 2, side_length / 2, 0])
        cube([sheet_indent * 2 + tolerance, sheet_indent * 2 + tolerance, sheet_width + tolerance], center=true);
    }
    
    // edge3 projection.
    rotate([90, 0, 90])
    intersection() {
        edge3(
            num_sections=2 + side_length / section_length,
            section_length=section_length,
            screw_diameter=screw_diameter,
            screw_tolerance=screw_tolerance,
            screw_length=screw_length,
            head_diameter=head_diameter,
            tolerance=tolerance,
            sheet_width=sheet_width,
            sheet_indent=sheet_indent,
            side_length=side_length,
            cut_side_length=cut_side_length,
            inside_cut_side_length=inside_cut_side_length,
            stem_side_length=stem_side_length,
            stem_length=stem_length,
            stem_stop_depth=stem_stop_depth,
            brace_thickness=brace_thickness
        );
        translate([0, 0, section_length + (sheet_width + tolerance) / 2]) cube([side_length + tolerance, side_length + tolerance, section_length * 2], center=true);
    }
    
    rotate([-90, 0, 0])
    intersection() {
        edge3(
            num_sections=2 + side_length / section_length,
            section_length=section_length,
            screw_diameter=screw_diameter,
            screw_tolerance=screw_tolerance,
            screw_length=screw_length,
            head_diameter=head_diameter,
            tolerance=tolerance,
            sheet_width=sheet_width,
            sheet_indent=sheet_indent,
            side_length=side_length,
            cut_side_length=cut_side_length,
            inside_cut_side_length=inside_cut_side_length,
            stem_side_length=stem_side_length,
            stem_length=stem_length,
            stem_stop_depth=stem_stop_depth,
            brace_thickness=brace_thickness
        );
        translate([0, 0, section_length + (sheet_width + tolerance) / 2]) cube([side_length + tolerance, side_length + tolerance, section_length * 2], center=true);
    }    
}


module connect3333(
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_tolerance=screw_tolerance,
    screw_length=screw_length,
    head_diameter=head_diameter,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
    brace_thickness=brace_thickness,
) {
    difference() {
        union() {
            edge3(
                num_sections=2 + side_length / section_length,
                section_length=section_length,
                screw_diameter=screw_diameter,
                screw_tolerance=screw_tolerance,
                screw_length=screw_length,
                head_diameter=head_diameter,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                brace_thickness=brace_thickness
            );
            
            rotate([90, 0, 0])
            edge3(
                num_sections=2 + side_length / section_length,
                section_length=section_length,
                screw_diameter=screw_diameter,
                screw_tolerance=screw_tolerance,
                screw_length=screw_length,
                head_diameter=head_diameter,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                brace_thickness=brace_thickness
            );
        }
        
        // Ensure space for the inserts past the union.
        for(a=[0:90:90]) {
            rotate([a, 0, 0])
            translate([(side_length - sheet_indent) / 2 , 0, 0])
            cube([sheet_indent + tolerance, sheet_width + tolerance, section_length], center=true);
        }
    }
}


module connect33334(
    section_length=section_length,
    screw_diameter=screw_diameter,
    screw_tolerance=screw_tolerance,
    screw_length=screw_length,
    head_diameter=head_diameter,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    cut_side_length=cut_side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
    brace_thickness=brace_thickness,
) {
    difference() {
        union() {
            edge3(
                num_sections=2 + side_length / section_length,
                section_length=section_length,
                screw_diameter=screw_diameter,
                screw_tolerance=screw_tolerance,
                screw_length=screw_length,
                head_diameter=head_diameter,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                brace_thickness=brace_thickness
            );
            
            rotate([90, 0, 0])
            edge3(
                num_sections=2 + side_length / section_length,
                section_length=section_length,
                screw_diameter=screw_diameter,
                screw_tolerance=screw_tolerance,
                screw_length=screw_length,
                head_diameter=head_diameter,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                cut_side_length=cut_side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth,
                brace_thickness=brace_thickness
            );            
        }
        
        // Ensure space for the inserts past the union.
        for(a=[0:90:90]) {
            rotate([a, 0, 0])
            translate([(side_length - sheet_indent) / 2 , 0, 0])
            cube([sheet_indent + tolerance, sheet_width + tolerance, section_length], center=true);
        }
    }
    
    // edge4 projection.
    rotate([0, 90, 0])
    intersection() {
        edge4(
            num_sections=2 + side_length / section_length,
            section_length=section_length,
            tolerance=tolerance,
            sheet_width=sheet_width,
            sheet_indent=sheet_indent,
            side_length=side_length,
            inside_cut_side_length=inside_cut_side_length,
            stem_side_length=stem_side_length,
            stem_length=stem_length,
            stem_stop_depth=stem_stop_depth
        );    
        translate([0, 0, section_length + (sheet_width + tolerance) / 2]) cube([side_length + tolerance, side_length + tolerance, section_length * 2], center=true);
    }
}


module connect4444(
    section_length=section_length,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
) {
    difference() {
        union() {
            edge4(
                num_sections=2 + side_length / section_length,
                section_length=section_length,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth
            );
            
            rotate([90, 0, 0])
            edge4(
                num_sections=2 + side_length / section_length,
                section_length=section_length,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth
            );
        }
        
        for(a2=[0:180:180]) {
            rotate([0, 0, a2]) 
            {
                // Ensure space for the inserts past the union.
                for(a=[0:90:90]) {
                    rotate([a, 0, 0])
                    translate([(side_length - sheet_indent) / 2 , 0, 0])
                    cube([sheet_indent + tolerance, sheet_width + tolerance, section_length], center=true);
                }
            }
        }
    }
}


module connect444444(
    section_length=section_length,
    tolerance=tolerance,
    sheet_width=sheet_width,
    sheet_indent=sheet_indent,
    side_length=side_length,
    inside_cut_side_length=inside_cut_side_length,
    stem_side_length=stem_side_length,
    stem_length=stem_length,
    stem_stop_depth=stem_stop_depth,
) {
    difference() {
        union() {
            edge4(
                num_sections=2 + side_length / section_length,
                section_length=section_length,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth
            );
            
            rotate([90, 0, 0])
            edge4(
                num_sections=2 + side_length / section_length,
                section_length=section_length,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth
            );
            
            rotate([0, 90, 0])
            edge4(
                num_sections=2 + side_length / section_length,
                section_length=section_length,
                tolerance=tolerance,
                sheet_width=sheet_width,
                sheet_indent=sheet_indent,
                side_length=side_length,
                inside_cut_side_length=inside_cut_side_length,
                stem_side_length=stem_side_length,
                stem_length=stem_length,
                stem_stop_depth=stem_stop_depth
            );
        }
        
        for(a3=[0:90:90]) {
            rotate([0, a3, 0]) {
                for(a2=[0:180:180]) {
                    rotate([0, 0, a2]) {
                        // Ensure space for the inserts past the union.
                        for(a=[0:90:270]) {
                            rotate([a, 0, 0])
                            translate([(side_length - sheet_indent) / 2 , 0, (side_length + section_length - tolerance) / 2 - sheet_indent])
                            cube([sheet_indent + tolerance, sheet_width + tolerance, section_length], center=true);
                        }
                    }
                }
            }
        }
    }
}


module insert_base(
    section_length=section_length,
    tolerance=tolerance,
    sheet_width=sheet_width,
    side_length=side_length,
    inside_cut_side_length=inside_cut_side_length,
    insert_length=insert_length,
    insert_thickness=insert_thickness,
    insert_retainer_length=insert_retainer_length,
    insert_retainer_depth=insert_retainer_depth,
    insert_base_thickness=insert_base_thickness,
    stem_side_length=stem_side_length,
    insert_addon_rotation=insert_addon_rotation,
) {
    semi_side = side_length / 2;
    semi_sheet = sheet_width / 2;
    semi_tolerance = tolerance / 2;
    semi_stem = stem_side_length / 2;
    
    delta = sin(22.5) * tolerance;
    diag = sqrt(2 * semi_side * semi_side) - sqrt(2 * inside_cut_side_length * inside_cut_side_length) / 2;
    sdiag = diag + tolerance + insert_base_thickness;
    pdiag = diag * cos(45);
    psdiag = sdiag * cos(45);
    intersect = 2 * pdiag - semi_sheet - semi_tolerance + (tolerance + insert_base_thickness) / cos(45);
    delta2 = (intersect - semi_side - tolerance) / 2;
    insert_first_depth = diag - (semi_stem - semi_tolerance) / cos(45);
    insert_depth = insert_first_depth + insert_retainer_depth;
    total_insert_depth = insert_depth + tolerance + semi_tolerance;
    
    // Profile extrusion for the base.
    linear_extrude(height=section_length - tolerance, center=true, convexity=3) {
        polygon(
            [
                [semi_sheet + semi_tolerance + delta2,       intersect - delta2],
                [semi_sheet + semi_tolerance,                semi_side + tolerance],
                [semi_sheet + semi_tolerance,                semi_side + tolerance],
                [semi_side - inside_cut_side_length + delta, semi_side + tolerance],
                [semi_side + tolerance,                      semi_side - inside_cut_side_length + delta],
                [semi_side + tolerance,                      semi_sheet + semi_tolerance],
                [intersect - delta2,                         semi_sheet + semi_tolerance + delta2],
            ]
        );
    }
    
    // Insert prongs.
    for(i=[-section_length / 4 : section_length / 2 : section_length / 4]) {
        translate([pdiag, pdiag, i - insert_retainer_length / 2]) {
            rotate([0, 0, 45]) {
                translate([-total_insert_depth / 2 + tolerance + semi_tolerance, 0, 0])
                cube([total_insert_depth, insert_thickness - tolerance, insert_length - insert_retainer_length - tolerance], center=true);
                
                translate([0, 0, -insert_retainer_length / 2])
                linear_extrude(height=insert_length - tolerance, center=true, convexity=3) {
                    polygon(
                        [
                            [-insert_first_depth, 0],
                            [-insert_first_depth - insert_thickness / 2 + semi_tolerance, insert_thickness / 2 - semi_tolerance],
                            [-insert_depth, insert_thickness / 2 - semi_tolerance],
                            [-insert_depth - (insert_thickness - tolerance) / 2, 0],
                            [-insert_depth, -insert_thickness / 2 + semi_tolerance],
                            [-insert_first_depth - insert_thickness / 2 + semi_tolerance, -insert_thickness / 2 + semi_tolerance],
                        ]
                    );
                }
            }
        }
    }
    
    // Children, centered on base surface.
    translate([psdiag, psdiag, 0]) rotate([0, 0, 45]) rotate([insert_addon_rotation, 0, 0]) {
        children();
    }    
}


function insert_base_width(side_length, sheet_width, tolerance) = sqrt(2) * ((side_length - sheet_width - tolerance) / 2  + tolerance);


module cable_tie_addon(
    base_width,
    tolerance=tolerance,
    cable_tie_hole_width=cable_tie_hole_width,
    cable_tie_hole_height=cable_tie_hole_height,
    cable_tie_holder_diameter=cable_tie_holder_diameter,
    cable_tie_holder_width=cable_tie_holder_width,
) {

    for(i=[-1:2:1]) {
        translate([0, i * (base_width - cable_tie_holder_width) / 2, 0]) {
            for(j=[-1:2:1]) {
                translate([0, 0, j * (cable_tie_hole_width + tolerance + cable_tie_holder_diameter) / 2]) {
                    translate([(tolerance / 2 + cable_tie_hole_height + cable_tie_holder_diameter / 2) / 2 - tolerance / 2, 0, 0])
                    cube([tolerance / 2 + cable_tie_hole_height + cable_tie_holder_diameter / 2, cable_tie_holder_width, cable_tie_holder_diameter], center=true);
                    
                    translate([(cable_tie_hole_height + cable_tie_holder_diameter / 2), 0, 0])
                    rotate([90, 0, 0])
                    cylinder(r=cable_tie_holder_diameter / 2, h = cable_tie_holder_width, center=true);
                }
            }
            
            translate([(cable_tie_hole_height + cable_tie_holder_diameter / 2), 0, 0])
            cube([cable_tie_holder_diameter, cable_tie_holder_width, cable_tie_holder_diameter + cable_tie_hole_width], center=true);
        }
    }  
}


module cable_tie_insert(
    section_length=section_length,
    tolerance=tolerance,
    sheet_width=sheet_width,
    side_length=side_length,
    inside_cut_side_length=inside_cut_side_length,
    insert_length=insert_length,
    insert_thickness=insert_thickness,
    insert_retainer_length=insert_retainer_length,
    insert_retainer_depth=insert_retainer_depth,
    insert_base_thickness=insert_base_thickness,
    stem_side_length=stem_side_length,
    cable_tie_hole_width=cable_tie_hole_width,
    cable_tie_hole_height=cable_tie_hole_height,
    cable_tie_holder_diameter=cable_tie_holder_diameter,
    cable_tie_holder_width=cable_tie_holder_width,
) {
    insert_base(
        section_length=section_length,
        tolerance=tolerance,
        sheet_width=sheet_width,
        side_length=side_length,
        inside_cut_side_length=inside_cut_side_length,
        insert_length=insert_length,
        insert_thickness=insert_thickness,
        insert_retainer_length=insert_retainer_length,
        insert_retainer_depth=insert_retainer_depth,
        insert_base_thickness=insert_base_thickness,
        stem_side_length=stem_side_length
    ) {
        base_width = insert_base_width(side_length, sheet_width, tolerance);
        cable_tie_addon(
            base_width,
            tolerance=tolerance,
            cable_tie_hole_width=cable_tie_hole_width,
            cable_tie_hole_height=cable_tie_hole_height,
            cable_tie_holder_diameter=cable_tie_holder_diameter,
            cable_tie_holder_width=cable_tie_holder_width
        );
    }
}


module point_up(dir) {
    dx = dir[0];
    dy = dir[1];
    dz = dir[2];
    
    if (printing_orientation) {
        rotate([atan2(sqrt(dx * dx + dy * dy), dz), 0, 0])
        rotate([0, 0, atan2(dx, dy)])
        children();
    } else {
        children();
    }
}


if(part_type == "stem") {
    point_up([1, 0, 0])
    stem();
} else if(part_type == "connect22") {
    point_up([1, 1, 0])
    connect22();
} else if(part_type == "connect33") {
    connect33();
} else if(part_type == "connect44") {
    connect44();
} else if(part_type == "connect222") {
    point_up([1, 1, 1])
    connect222();
} else if(part_type == "connect2233") {
    point_up([1, 1, 0])    
    connect2233();
} else if(part_type == "connect3333") {
    point_up([1, 0, 0])    
    connect3333();
} else if(part_type == "connect4444") {
    point_up([0, 1, 1])    
    connect4444();
} else if(part_type == "connect33334") {
    point_up([1, 0, 0])        
    connect33334();
} else if(part_type == "connect444444") {
    point_up([1, 1, 1])    
    connect444444();
} else if(part_type == "brace2") {
    brace2();
} else if(part_type == "stop2") {
    brace2(stop=true);
} else if(part_type == "hinge2") {
    brace2(hinge=true);
} else if(part_type == "brace3") {
    brace3();
} else if(part_type == "edge2") {
    point_up([1, 1, 0])
    edge2();
} else if(part_type == "edge3") {
    point_up([1, 0, 0])    
    edge3();
} else if(part_type == "edge4") {
    edge4();
} else if(part_type == "cable_tie_insert") {
    point_up([-1, 1, 0])        
    cable_tie_insert();
} else if(part_type == "test") {
}